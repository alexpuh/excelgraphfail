﻿using System.Collections.Generic;

namespace excelgraphfail
{
    class Program
    {
        static void Main()
        {
            const string fileName = @"c:\temp\test-data.xlsx";
            CreateDocumentExample.CreateSpreadsheetWorkbook(fileName);
            var testData = new Dictionary<string, int> {{"Monday", 1}, {"Tuesday", 10}, {"Wednesday", 4}};
            InsertChartExample.InsertChartInSpreadsheet(fileName, "mySheet", "Created Chart", testData);
        }
    }
}
